var fs = Npm.require("fs");
var path = Npm.require("path");
var typescript = Npm.require("typescript");

var sourceMapReferenceLineRegExp = new RegExp("//# sourceMappingURL=.*$", "m");

function compile(input) {
	var result = {
		source: "",
		sourceMap: "",
		errors: [],
		nonfatalErrors: [],  // custom field
	};
	var errorsToConvert;
	var haveFatalErrors;

	compileBlock: {
		// NOTE: There is no detection of changes to tsconfig.json; the developer
		// has to restart "meteor". :(
		var rcfResult = typescript.readConfigFile(input.tsconfigPath,
			(path) => fs.readFileSync(path, { encoding: "utf8" })
			);
		var configObject = rcfResult.config;
		if (!configObject) {
			errorsToConvert = [rcfResult.error];
			haveFatalErrors = true;
			break compileBlock;
		}
		var configParseResult = typescript.parseJsonConfigFileContent(
			configObject, typescript.sys, typescript.getDirectoryPath(input.tsconfigPath),
			{}, input.tsconfigPath);
		if (configParseResult.errors.length > 0) {
			errorsToConvert = configParseResult.errors;
			haveFatalErrors = true;
			break compileBlock;
		}

		var options = configParseResult.options;
		// Emit-specific options.
		options.noEmit = false;
		options.out = "out.js";
		options.sourceMap = true;
		options.removeComments = true;

		var compilerHost = typescript.createCompilerHost(options);
		compilerHost.writeFile = function(fileName, data, writeByteOrderMark, onError) {
			if (fileName == "out.js")
				result.source = data;
			else
				result.sourceMap = data;
		};

		var program = typescript.createProgram(input.fullPaths, options, compilerHost);
		var emitResult = program.emit();

		//var allDiagnostics = typescript.getPreEmitDiagnostics(program).concat(emitResult.diagnostics);
		var syntacticDiagnostics = program.getSyntacticDiagnostics();
		var semanticDiagnostics = program.getSemanticDiagnostics();

		errorsToConvert = syntacticDiagnostics.concat(semanticDiagnostics);
		haveFatalErrors = (syntacticDiagnostics.length > 0);

		if (!emitResult.emitSkipped) {
			// Remove source map reference line from generated js file.
			// Meteor sets up source map through HTTP response header instead.
			// FIXME: Should be an option for TypeScript compiler.
			result.source = result.source.replace(sourceMapReferenceLineRegExp, "");

			// FIXME: Embed sources directly in the source map, as there is no way to make Meteor serve them as files.
			sourceMapObject = JSON.parse(result.sourceMap);
			sourceMapObject.file = input.pathForSourceMap;
			sourceMapObject.sourcesContent = [];
			sourceMapObject.sources.forEach(function(sourcePath) {
				var fullPath = path.join(process.cwd(), sourcePath);
				var sourceContent = fs.readFileSync(fullPath, { encoding: "utf8" });
				sourceMapObject.sourcesContent.push(sourceContent);
			});
			result.sourceMap = JSON.stringify(sourceMapObject);
		}

		// Consider running the linter here if there were no errors?
		// That will lengthen the edit-compile-run cycle for everyone,
		// in contrast to "check-code", which can just be run once after
		// the code is finished.
	}

	errorsToConvert.forEach(function(diagnostic) {
		var sourcePath = null, line = null, character = null;
		if (diagnostic.file) {
			sourcePath = diagnostic.file.fileName;
		  var lineAndCharacter = diagnostic.file.getLineAndCharacterOfPosition(diagnostic.start);
		  line = lineAndCharacter.line + 1;
		  character = lineAndCharacter.character + 1;
		}
		var diagnosticCategory = typescript.DiagnosticCategory[diagnostic.category];
		var message = typescript.flattenDiagnosticMessageText(diagnostic.messageText, "\n");

		var error = {
			message: diagnosticCategory + " TS" + diagnostic.code + ": " + message,
			sourcePath: sourcePath,
			line: line,
			column: character
		};
		if (haveFatalErrors)
			result.errors.push(error);
		else
			result.nonfatalErrors.push(error);
	});

	return result;
}

// Cache persists across plugin invocations.
this.cache = this.cache || {};

function cachedCompile(input) {
	var key = "";

	input.fullPaths && input.fullPaths.forEach(function(fullPath) {
		key += fullPath;
		key += ":";
		key += fs.statSync(fullPath).mtime.getTime();
		key += ":";
	});

	var archCache = cache[input.arch] || {};

	if (archCache.key !== key) {
		archCache.key = key;
		archCache.result = compile(input);
	}

	// Re-display the errors on a cache hit.  This could be distracting, but it's
	// less bad than letting the user think the errors are gone.  (A user who
	// remembers whether they changed a TypeScript file might be able to avoid
	// getting confused, but this way if you see two successive "Meteor server
	// restarted" or "Client modified -- refreshing" messages without intervening
	// errors, you can pretty much conclude there are no errors.  I tried a bunch
	// of cases and didn't find any that violated this rule. ~ Matt 2016-09-19)
	archCache.result.nonfatalErrors.forEach(function(error) {
		console.log(error.sourcePath + " (" + error.line + ", " + error.column + "): " + error.message);
	});

	cache[input.arch] = archCache;
	return archCache.result;
}

var compileInput = {};

Plugin.registerSourceHandler("ts", function(compileStep) {
	// Somewhere between Meteor 1.2.1 and 1.4.1.1, the files in definitions/
	// started being considered source files of the hansoft:typescript package,
	// which means they get passed here, which caused them to be included in the
	// compilation of the app.  Clearly the hack this plugin uses to collect files
	// is completely broken when packages are involved.  The following will at
	// least get us back in business.  Better would be to port the plugin to the
	// registerCompiler API, but first we might want to recheck for a workaround
	// that would allow us to use the official TypeScript build plugin.
	// ~ Matt 2016-09-19
	if (compileStep.packageName != null)
		return;

	compileInput.fullPaths = compileInput.fullPaths || [];
	compileInput.fullPaths.push(compileStep.fullInputPath);
});

Plugin.registerSourceHandler("ts-build", function(compileStep) {
	if (compileStep.packageName != null)
		return;

	compileInput.tsconfigPath =
		compileStep.fullInputPath.substring(0, compileStep.fullInputPath.length - compileStep.inputPath.length)
		+ 'tsconfig.json';
	compileInput.arch = compileStep.arch;
	compileInput.pathForSourceMap = compileStep.pathForSourceMap;

	var result = cachedCompile(compileInput);

	result.errors.forEach(function(error) {
		compileStep.error(error);
	});

	if (result.source) {
		compileStep.addJavaScript({
			path: compileStep.inputPath + ".js",
			sourcePath: compileStep.inputPath,
			data: result.source,
			sourceMap: result.sourceMap,
		});
	}

	compileInput = {};
});
